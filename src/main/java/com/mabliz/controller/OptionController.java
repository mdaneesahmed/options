package com.mabliz.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mabliz.entities.OptionLookup;
import com.mabliz.repo.OptionLookupRepo;


@RestController("/option")
public class OptionController {

	@Autowired
	OptionLookupRepo optionRepo;
	
	@GetMapping("/setoptiondisabled")
	public @ResponseBody OptionLookup setOptionLookupDisabled(@RequestParam("optionid") Long optionId, Boolean status) {
		
		OptionLookup option = optionRepo.findOne(optionId);
		//System.out.println("OptionLookup Name : " + option.getName());
		if(option==null)
			return null;
		//option.set(status);
		optionRepo.save(option);
		return option;
	}
	
	
	@GetMapping("/setprodutdeactive")
	public @ResponseBody OptionLookup setOptionLookupDeactive(@RequestParam("optionid") Long optionId, int active) {
		
		OptionLookup option = optionRepo.findOne(optionId);
		//System.out.println("OptionLookup Name : " + option.getName());
		if(option==null)
			return null;
		option.setActive(active);
		optionRepo.save(option);
		return option;
	}
	
	
	
	
	
	@GetMapping("/deactiveoptionsbycategory")
	public @ResponseBody  List<OptionLookup> deactvieOptionLookupByCategory(@RequestParam("industryid") Long industryId,@RequestParam("category") String category, @RequestParam("active") int active) {
			List<OptionLookup> cateogories = optionRepo.findByCategoryAndIndustryId(category, industryId);
			for (OptionLookup optionLookup : cateogories) {
				optionLookup.setActive(active);
			}
			optionRepo.save(cateogories);
		return cateogories;
	}
	
	
}
