package com.mabliz.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mabliz.entities.OptionLookup;

@Repository
public interface OptionLookupRepo extends JpaRepository<OptionLookup, Long>{
	
	public List<OptionLookup> findByIndustryId(@Param("industryId") Long industryId);
	public List<OptionLookup> findByCategoryAndIndustryId(@Param("category")  String category,@Param("industryId") Long industryId);
	public List<OptionLookup> findByPkeyAndIndustryId(@Param("pkey") String pkey,@Param("industryId") Long industryId);
	public List<OptionLookup> findByCategory(@Param("category")  String category);
	public List<OptionLookup> findByPkey(@Param("pkey") String pkey);

}
