package com.mabliz.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception Handler for custom exceptions for PRISM MS project
 * 
 * @author Dan Duncan (dd576v)
 * @version 1.0
 * @since 2018-08-12
 * @see ResponseEntityExceptionHandler
 *
 */
@ControllerAdvice
@RestController
public class MarmufExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(MarmufException.class)
	public final ResponseEntity<ErrorResponse> handlePrismMSException(MarmufException ex, WebRequest request) {
		ErrorResponse errorDetails = new ErrorResponse(ex.getErrorCode(), ex.getErrorMessage(), ex.getDetails());
		ex.printStackTrace();

		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		try {
			status = HttpStatus.valueOf(ex.getErrorCode());
		} catch (java.lang.IllegalArgumentException e) {
			// Do nothing - status is already set.
		}

		return new ResponseEntity<ErrorResponse>(errorDetails, status);
	}

}
