package com.mabliz.errorhandler;

public enum ResponseEnum {  
	// 1xx_INFORMATIONAL
	C100_CONTINUE (100, "Continue in '%s'"),
	C101_SWITCHING_PROTOCOLS (101, "Switching Protocols in '%s'"),
	C102_PROCESSING (102, "Processing in '%s'"),
	// 2xx_SUCCESS
	C200_OK (200, "OK in '%s'"),
	C201_CREATED (201, "Created in '%s'"),
	C202_ACCEPTED (202, "Accepted in '%s'"),
	C203_NON_AUTHORITATIVE_INFORMATION (203, "Non-Authoritative Information in '%s'"),
	C204_NO_CONTENT (204, "No Content in '%s'"),
	C205_RESET_CONTENT (205, "Reset Content in '%s'"),
	C206_PARTIAL_CONTENT (206, "Partial Content in '%s'"),
	C207_MULTI_STATUS (207, "Multi-Status in '%s'"),
	C208_ALREADY_REPORTED (208, "Already Reported in '%s'"),
	C226_IM_USED (226, "IM Used in '%s'"),
	// 3xx_REDIRECTION
	C300_MULTIPLE_CHOICES (300, "Multiple Choices in '%s'"),
	C301_MOVED_PERMANENTLY (301, "Moved Permanently in '%s'"),
	C302_FOUND (302, "Found in '%s'"),
	C303_SEE_OTHER (303, "See Other in '%s'"),
	C304_NOT_MODIFIED (304, "Not Modified in '%s'"),
	C305_USE_PROXY (305, "Use Proxy in '%s'"),
	C307_TEMPORARY_REDIRECT (307, "Temporary Redirect in '%s'"),
	C308_PERMANENT_REDIRECT (308, "Permanent Redirect in '%s'"),
	// 4xx_CLIENT_ERROR
	C400_BAD_REQUEST (400, "Bad Request in '%s'"),
	C401_UNAUTHORIZED (401, "Unauthorized in '%s'"),
	C402_PAYMENT_REQUIRED (402, "Payment Required in '%s'"),
	C403_FORBIDDEN (403, "Forbidden in '%s'"),
	C404_NOT_FOUND (404, "Not Found in '%s'"),
	C405_METHOD_NOT_ALLOWED (405, "Method Not Allowed in '%s'"),
	C406_NOT_ACCEPTABLE (406, "Not Acceptable in '%s'"),
	C407_PROXY_AUTHENTICATION_REQUIRED (407, "Proxy Authentication Required in '%s'"),
	C408_REQUEST_TIMEOUT (408, "Request Timeout in '%s'"),
	C409_CONFLICT (409, "Conflict in '%s'"),
	C410_GONE (410, "Gone in '%s'"),
	C411_LENGTH_REQUIRED (411, "Length Required in '%s'"),
	C412_PRECONDITION_FAILED (412, "Precondition Failed in '%s'"),
	C413_REQUEST_ENTITY_TOO_LARGE (413, "Request Entity Too Large in '%s'"),
	C414_REQUEST_URI_TOO_LONG (414, "Request-URI Too Long in '%s'"),
	C415_UNSUPPORTED_MEDIA_TYPE (415, "Unsupported Media Type in '%s'"),
	C416_REQUESTED_RANGE_NOT_SATISFIABLE (416, "Requested Range Not Satisfiable in '%s'"),
	C417_EXPECTATION_FAILED (417, "Expectation Failed in '%s'"),
	C418_IM_A_TEAPOT (418, "I'm a teapot in '%s'"),
	C420_ENHANCE_YOUR_CALM (420, "Enhance Your Calm in '%s'"),
	C422_UNPROCESSABLE_ENTITY (422, "Unprocessable Entity in '%s'"),
	C423_LOCKED (423, "Locked in '%s'"),
	C424_FAILED_DEPENDENCY (424, "Failed Dependency in '%s'"),
	C425_RESERVED_FOR_WEBDAV (425, "Reserved for WebDAV in '%s'"),
	C426_UPGRADE_REQUIRED (426, "Upgrade Required in '%s'"),
	C428_PRECONDITION_REQUIRED (428, "Precondition Required in '%s'"),
	C429_TOO_MANY_REQUESTS (429, "Too Many Requests in '%s'"),
	C431_REQUEST_HEADER_FIELDS_TOO_LARGE (431, "Request Header Fields Too Large in '%s'"),
	C444_NO_RESPONSE (444, "No Response in '%s'"),
	C449_RETRY_WITH (449, "Retry With in '%s'"),
	C450_BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS (450, "Blocked by Windows Parental Controls in '%s'"),
	C451_UNAVAILABLE_FOR_LEGAL_REASONS (451, "Unavailable For Legal Reasons in '%s'"),
	C499_CLIENT_CLOSED_REQUEST (499, "Client Closed Request in '%s'"),
	// 5xx_SERVER_ERROR
	C500_INTERNAL_SERVER_ERROR (500, "Internal Server Error in '%s'"),
	C501_NOT_IMPLEMENTED (501, "Not Implemented in '%s'"),
	C502_BAD_GATEWAY (502, "Bad Gateway in '%s'"),
	C503_SERVICE_UNAVAILABLE (503, "Service Unavailable in '%s'"),
	C504_GATEWAY_TIMEOUT (504, "Gateway Timeout in '%s'"),
	C505_HTTP_VERSION_NOT_SUPPORTED (505, "HTTP Version Not Supported in '%s'"),
	C506_VARIANT_ALSO_NEGOTIATES (506, "Variant Also Negotiates in '%s'"),
	C507_INSUFFICIENT_STORAGE (507, "Insufficient Storage in '%s'"),
	C508_LOOP_DETECTED (508, "Loop Detected in '%s'"),
	C509_BANDWIDTH_LIMIT_EXCEEDED (509, "Bandwidth Limit Exceeded in '%s'"),
	C510_NOT_EXTENDED (510, "Not Extended in '%s'"),
	C511_NETWORK_AUTHENTICATION_REQUIRED (511, "Network Authentication Required in '%s'"),
	C598_NETWORK_READ_TIMEOUT_ERROR (598, "Network read timeout error in '%s'"),
	C599_NETWORK_CONNECT_TIMEOUT_ERROR (599, "Network connect timeout error in '%s'"),
	
	// 6xx_CUSTOM ERROR
	C600_NO_RECORD_FOUND_ERROR (600, "No Record Found"),
	C601_USER_ALREADY_EXIST (601, "User id '%s' provided  is already exists. Please choose another userId"),
	C602_OPTIONSLOOKUP_ALREADY_EXISTS (602, "Options Lookup Value '%s' provided  is already exists."),
	C603_OPTIONSKEY_ALREADY_EXISTS (603, "Options Key '%s' provided  is already exists."),
	C604_NO_RECORD_FOUND_INSERT_ERROR (604, "No Records found for insertion"),
	C605_LICENSE_EXPIRED (605, "License expired. Kindly Renewal the License."),
	C606_OPENBALANCE_ALREADY_EXIST (606, "Open Balance Already Exist Close Balance First"),
	C607_CLOSEBALANCE_ALREADY_COMPLTED (607, "Nothing to Close, Open Balance Already Closed");
	
	

    private final int responseCode;  
    private final String responseMessage; 
    
    ResponseEnum(int responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

	public int getResponseCode() {
		return responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}
}
    