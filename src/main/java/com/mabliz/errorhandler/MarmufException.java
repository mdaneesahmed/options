package com.mabliz.errorhandler;

import java.io.PrintWriter;
import java.io.StringWriter;

public class MarmufException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;
	private int errorCode;
	private String methodName = "";
	private String details = "";

	public MarmufException(String message){
		super(message);
		
	}
	public MarmufException(ResponseEnum responseEnum, String methodName, String replaceString) {
		this.methodName = methodName;
		this.errorMessage = String.format(responseEnum.getResponseMessage().replace("'%s'", replaceString), methodName);
		this.errorCode = responseEnum.getResponseCode();
	}

	public MarmufException(Exception exception, Object object, String message) {
		super(exception.getMessage(), exception);
		exception.printStackTrace();
		this.setStackTrace(exception.getStackTrace());
		this.methodName = (object != null) ? object.getClass().getEnclosingMethod().getName() : "UNKNOWN";
		this.errorMessage = (exception instanceof MarmufException) ? ((MarmufException) exception).getErrorMessage()
				: String.format(ResponseEnum.C500_INTERNAL_SERVER_ERROR.getResponseMessage(), methodName);
		this.errorCode = (exception instanceof MarmufException) ? ((MarmufException) exception).getErrorCode()
				: ResponseEnum.C500_INTERNAL_SERVER_ERROR.getResponseCode();
	}

	public MarmufException() {
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	public String getExceptionDetails() {
		return getExceptionDetailsFromStackTrace();
	}

	private String getExceptionDetailsFromStackTrace() {
		// StackTraceElement[] stackTrace = this.getStackTrace();
		// if(stackTrace == null || stackTrace.length == 0) return "";
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		this.printStackTrace(printWriter);
		return stringWriter.toString();
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public MarmufException setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		return this;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public MarmufException setErrorCode(int errorCode) {
		this.errorCode = errorCode;
		return this;
	}

	/**
	 * @param methodName
	 *            the methodName to set
	 */
	public MarmufException setMethodName(String methodName) {
		this.methodName = methodName;
		return this;
	}

	/**
	 * @param details
	 *            the details to set
	 */
	public MarmufException setDetails(String details) {
		this.details = details;
		return this;
	}

}
