package com.mabliz.errorhandler;

import java.util.Date;

/**
 * @author dd576v
 *
 */
public class ErrorResponse {

	private Date timestamp;
	private int errorCode;
	private String message;
	private String exceptionDetails;
	
	
	public ErrorResponse() {
		super();
	}

	public ErrorResponse(int errorCode, String message, String exceptionDetails) {
		super();
		this.timestamp= new Date();
		this.errorCode = errorCode;
		this.message = message;
		this.exceptionDetails = exceptionDetails;
	}

	public ErrorResponse(Date timestamp, int errorCode, String message, String exceptionDetails) {
		super();
		this.timestamp = timestamp;
		this.errorCode = errorCode;
		this.message = message;
		this.exceptionDetails = exceptionDetails;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public int getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	

	public String getExceptionDetails() {
		return exceptionDetails;
	}

	public void setExceptionDetails(String exceptionDetails) {
		this.exceptionDetails = exceptionDetails;
	}
}