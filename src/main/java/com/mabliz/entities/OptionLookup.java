package com.mabliz.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

	
@Entity
public class OptionLookup implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	// disabled or enabled.. based 1 is enable 0 is disabled. 
	private int active;	
	
	//
	private String category; // This will show what sort of dropdown you are going to use.. indType = Industry, etc.. if you are using it for Countries.. 
	//then category CountryType key is IN and Value is INDIA, country type always same.. but key and values will change..
	// Same if you are using it for States.. category is StateType and  key is TN value is Tamilnadu and pakey is IN 
	private String mkey; // Key
	private String value; // Value
	private String pkey; // Parent Key
	private String imagePath; // Image path value
	
	private Long industryId;
	
	
	
	
	public Long getIndustryId() {
		return industryId;
	}
	public void setIndustryId(Long industryId) {
		this.industryId = industryId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	
	
	
	public String getMkey() {
		return mkey;
	}
	public void setMkey(String mkey) {
		this.mkey = mkey;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getPkey() {
		return pkey;
	}
	public void setPkey(String pkey) {
		this.pkey = pkey;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
}
